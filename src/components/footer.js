import React from "react"
import styled from "styled-components"
import { Container, Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faTwitter, faGooglePlusG } from "@fortawesome/free-brands-svg-icons"
import { Link } from "gatsby";

const TopFooterStyles = styled.div`
  display: flex;
  width: 100%;
  background: #1A1A1A;
`;
const FooterStyles = styled.div`
  display: flex;
  width: 100%;
  background: #f5f5f5;
`;
const FooterRow = styled(Row)`
  margin-top: 20px;
  margin-bottom: 10px;
`;
const Div = styled.footer`
  width: 80%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.6rem;
`;

const Footer = () => {
  return (
    <div>
      <FooterStyles>
        <Container>
          <FooterRow>
            <Col lg={4}>
              ที่อยู่
              <hr style={{width: 35}}/>
              <p>อาคารบัณฑิตแนะแนว ชั้น 6 เลขที่ 1033/4 ถนนพหลโยธิน แขวงพญาไท เขตพญาไท กรุงเทพ ฯ 10400</p>
            </Col>
            <Col lg={2}>
              เวลาทำการ
              <hr style={{width: 82}}/>
              <p>วันจันทร์ - วันศุกร์<br/>
                10:00 น. - 19.00 น.
              </p>
              </Col>
              <Col lg={3}>
              ติดต่อ
              <hr style={{width: 48}}/>
              <p>
                เบอร์โทรศัพท์: (66) 0 2619 8833 <br/>
                อีเมล: contact@gurusquare.com
              </p>
              </Col>
            <Col lg={3}>
              ติดตามพวกเรา
              <hr style={{width: 110}}/>
              <p>
                <Link to=''> <FontAwesomeIcon icon={faFacebookF} size="lg"/> </Link>
                <Link to=''> <FontAwesomeIcon icon={faTwitter} size="lg"/> </Link>
                <Link to=''> <FontAwesomeIcon icon={faGooglePlusG} size="lg"/> </Link>
              </p>
              </Col>
          </FooterRow>
        </Container>
      </FooterStyles>
      <TopFooter />
    </div>
    
  )
}

const TopFooter = () => {
  return (
    <TopFooterStyles>
      <Container style={{color: 'white', textAlign: "center", marginTop: '20px' }}>
        <Row>
          <Col>
            <p >
              © 2019 Copyright: KP-Trading-Group.com
            </p>
          </Col>
        </Row>
      </Container>
   </TopFooterStyles>
  )
}

export default Footer