import PropTypes from "prop-types"
import React from "react"
import { StaticQuery, Link, graphql } from "gatsby"
import { Navbar, Nav, NavDropdown, Image } from "react-bootstrap"
import styled from "styled-components"
// Images
import logo from "../images/logo.png"
// CSS
import "bootstrap/dist/css/bootstrap.css"

const NavbarComponent = styled(Navbar)`
  display: flex;
  background: white;
`

const NavbarBrandComponent = styled(Navbar.Brand)`
  width: 260px;
  height: 55px;
  margin-left: 10px;

  @media only screen and (min-width: 1068px) {
    margin-left: 50px;
  }
`

const NavLinkComponent = styled(Nav.Link)`
  color: red;
`

const NavDropdownComponent = styled(NavDropdown)`
  &:hover {
    display: block;
  }
`

const linkStyles = {}

const activeStyles = {
  color: "black",
}

class Header extends React.Component {
  constructor(props) {
    console.log(props)
    super(props)
    // this.data = props.data
    this.state = { isOpen: false }
  }

  handleOpen = () => {
    this.setState({ isOpen: true })
    console.log("right now state isOpen is ", this.state.isOpen, " changing...")
  }

  handleClose = () => {
    this.setState({ isOpen: false })
    console.log("right now state isOpen is ", this.state.isOpen, " changing...")
  }

  render() {
    console.log('I was triggered during render')
    // console.log(this.data)
    console.log(this.state.isOpen)
    return (
      <NavbarComponent bg="light" expand="lg" sticky="top">
        <NavbarBrandComponent href="/">{/* HREF IS NOT GOOD */}
          <Image src={logo} />
        </NavbarBrandComponent>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav>
            <Link to="/products" className="nav-link" style={linkStyles} activeStyle={activeStyles}>สินค้า</Link> {/* Product */}
            <NavDropdownComponent title="ประเภทสินค้า" id="nav-dropdown" className="nav-link" style={linkStyles} activeStyle={activeStyles} onMouseEnter={this.handleOpen} onMouseLeave={this.handleClose} show={this.state.isOpen}>
              {/* Industry */}
              <Link to="/page-2" className="nav-link" style={linkStyles} activeStyle={activeStyles}>อินเตอร์เน็ต</Link>
              <Link to="/page-2" className="nav-link" style={linkStyles} activeStyle={activeStyles}>Test {this.props.data.allProductJson.edges[0].node.name}</Link>
              <NavDropdown.Divider />
            </NavDropdownComponent>
            <Link to="/partners" className="nav-link" style={linkStyles} activeStyle={activeStyles}>ลูกค้า</Link> {/* Partner */}
            <Link to="/about" className="nav-link" style={linkStyles} activeStyle={activeStyles}>เกี่ยวกับเรา</Link>{/* About Us */}
            <Link to="/contact" className="nav-link" style={linkStyles} activeStyle={activeStyles}>ติดต่อเรา</Link>{/* Contact Us */}
          </Nav>
        </Navbar.Collapse>
      </NavbarComponent>
    )
  }
}

export default props => (
  <StaticQuery
    query={graphql`
    query {
      allProductJson {
        edges {
          node {
            id
            name
            price
          }
        }
      }
    }
  `}
  render={data => <Header data={data} {...props}/>}
  />
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}