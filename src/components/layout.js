/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import styled, {ThemeProvider, createGlobalStyle} from 'styled-components'

import Header from "./header"
import Footer from "./footer"
import "./css/layout.css"


const theme = {
  teal: "#009688",
  darkTeal: "#00796B",
  lightTeal: "#B2DFDB",
  black: "#212121",
  grey: "#757575",
  lightGrey: "#BDBDBD",
  white: "#FFFFFF",
  red: "#FF5252",
  darkRed: "#D32F2F",
  orange: "#FF5722",
  blue: "#466ab5",
  lightBlue: "#1976d2",
  darkBlue: "#3b5998",
  sm: "(max-width: 600px)",
  md: "(max-width: 960px)",
  lg: "(max-width: 1025px)",
  xl: "(min-width: 1026px)",
  width: "1000px",
  fontFamily: "Operator Mono",
  transitionDuration: "0.35s",
}

// font-family: ${props => props.theme.fontFamily};
const GlobalStyle = createGlobalStyle`
  html {
    box-sizing: border-box;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    padding: 0;
    margin: 0;
    color: ${props => props.theme.black};
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
  }
  p {
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-size: 0.85rem;
  }
   h1 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 2.25rem;
    line-height: 1.1;
  }
  h2 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 1.62671rem;
    line-height: 1.1;
  }
  h3 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 1.38316rem;
    line-height: 1.1;
  }
  h4 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 1rem;
    line-height: 1.1;
  }
  h5 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 0.85028rem;
    line-height: 1.1;
  }
  h6 {
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
    margin-bottom: 1.45rem;
    color: inherit;
    @import url('https://fonts.googleapis.com/css?family=Prompt:200&display=swap');
    font-family: Prompt !important;
    font-weight: bold;
    text-rendering: optimizeLegibility;
    font-size: 0.78405rem;
    line-height: 1.1;
  }
`
const Page = styled.div`
  margin: 0;
  padding: 0;
`


const Content = styled.main`
  width: 100%;
  margin: 0 auto;
`

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
  <ThemeProvider theme={theme}>
    <Page>
      <GlobalStyle />
      <Header siteTitle={data.site.siteMetadata.title} />
      <Content>
          {children}
      </Content>
      <Footer/>

    </Page>   
  </ThemeProvider>
    
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
