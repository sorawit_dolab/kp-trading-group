import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

import { Carousel, Container, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import sideImage from '../images/pic1.png';
import sideImage2 from '../images/pic2.png';
import sideImage3 from '../images/pic3.png';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Container>
      <Row>
        <h1>เกี่ยวกับเรา</h1>
      </Row>
      <Row>
      <h2>สายเคเบิล</h2>
      </Row>
      <Row>
      <p>เกี่ยวกับเรา</p>
      </Row>
      <Row>
      <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
          <Image />
        </div>
      </Row>
      <Row>
        <Link to="/page-2/">ไปที่หน้า 2</Link>
      </Row>
    </Container>

    
  </Layout>
)

export default IndexPage
