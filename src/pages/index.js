import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
// import Image from "../components/image"
import SEO from "../components/seo"

import { Carousel, Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import sideImage from '../images/pic1.png';
import sideImage2 from '../images/pic2.png';
import sideImage3 from '../images/pic3.png';

import '../components/css/landing-page.css';

import '../components/css/fontawesome-free/css/all.min.css';
import '../components/css/simple-line-icons/css/simple-line-icons.css';
import styled from "styled-components"

const TitleCeneterStyles = styled.h3`
  margin-top: 40px;
  text-align: center;
`;

const divStyles = {
  marginTop: '40px'
}

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <CarouselComponent />
    <ThreeMainComponent />    
    <ResourcesComponent />
    <WhoIsComponent />
  </Layout>
)

const WhoIsComponent = () => (
  <Container>
    <Row lg={6} style={divStyles}>
      <Col>
        <h3>KP-Tradning-Group คือใคร</h3>
        <p>ระบบบัญชีใช้งานง่าย เปิดบิล บันทึกค่าใช้จ่าย ทำบัญชี และรายงานภาษี สำหรับธุรกิจที่เพิ่งเริ่มต้น ครบจบในที่เดียว</p>
        <ul>
          <li>
            <strong>สายไฟที่ดีที่สุดในไทย</strong>
          </li>
          <li>เปิดบิลขาย</li>
          <li>ทำเอกสารคล่อง</li>
          <li>ลดงานทางบัญชีและภาษี</li>
        </ul>
        <p>แม้จะเป็นเจ้าของธุรกิจคนเดียวก็สามารถทำงานใหญ่ได้ด้วยโปรแกรมบัญชี คลาวด์ ที่เชื่อมโยงฐานข้อมูลจากในเว็บไซต์ และมือถือ ให้คุณสามารถเปิดบิลได้ครบ และบันทึกค่าใช้จ่ายระหว่างที่อยู่นอกออฟฟิศได้เลย</p>
      </Col>
      <Col lg={6}>
        <img className="img-fluid rounded" src="http://placehold.it/700x450" alt="" />
        {/* <img classNameName="img-fluid rounded" src={sideImage} alt="First slide" /> */}
      </Col>
    </Row>
  </Container>
)

const CarouselComponent = () => (
  <Carousel>
      <Carousel.Item>
        <img className="d-block w-100" src={sideImage} alt="First slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={sideImage2} alt="Third slide" />
      </Carousel.Item>
      <Carousel.Item>
        <img className="d-block w-100" src={sideImage3} alt="Third slide" />
      </Carousel.Item>
    </Carousel>
)

const ThreeMainComponent = () => (
  <Container>
    <Row className="features-icons text-center" >
      <Col lg={4}>
          <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div className="features-icons-icon d-flex">
              <i className="icon-check m-auto text-primary"></i>
            </div>
            <h3>ความน่าเชื่อถือ</h3>
            <p >ดีไซน์แบบหน้าจอทั้งหมด แบตเตอรี่ที่ใช้งานได้นานที่สุดเท่าที่เคยมีใน iPhone ประสิทธิภาพการทำงานที่เร็วที่สุดอีกทั้งยังทนน้ำและน้ำที่กระเด็นใส่ ภาพถ่ายคุณภาพระดับสตูดิโอและวิดีโอ 4K รวมถึงความปลอดภัยที่มากขึ้นด้วย Face ID </p>
          </div>
        </Col>
        <Col lg={4}>
          <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
            <div className="features-icons-icon d-flex">
              <i className="icon-location-pin m-auto text-primary"></i>
            </div>
            <h3>ผลิตที่ประเทศไทย</h3>
            <p>ดีไซน์แบบหน้าจอทั้งหมด แบตเตอรี่ที่ใช้งานได้นานที่สุดเท่าที่เคยมีใน iPhone ประสิทธิภาพการทำงานที่เร็วที่สุดอีกทั้งยังทนน้ำและน้ำที่กระเด็นใส่ ภาพถ่ายคุณภาพระดับสตูดิโอและวิดีโอ 4K รวมถึงความปลอดภัยที่มากขึ้นด้วย Face ID </p>
          </div>
        </Col>
        <Col lg={4}>
          <div className="features-icons-item mx-auto mb-0 mb-lg-3">
            <div className="features-icons-icon d-flex">
              <i className="icon-badge m-auto text-primary"></i>
            </div>
            <h3>คุณภาพสูง</h3>
            <p>ดีไซน์แบบหน้าจอทั้งหมด แบตเตอรี่ที่ใช้งานได้นานที่สุดเท่าที่เคยมีใน iPhone ประสิทธิภาพการทำงานที่เร็วที่สุดอีกทั้งยังทนน้ำและน้ำที่กระเด็นใส่ ภาพถ่ายคุณภาพระดับสตูดิโอและวิดีโอ 4K รวมถึงความปลอดภัยที่มากขึ้นด้วย Face ID </p>
          </div>
        </Col>
    </Row>
  </Container>
  
)

const ResourcesComponent = () => (
  <div className="bg-light">
    <Container>
      <Row className="features-icons text-center">
        <Col lg={12}>
          <TitleCeneterStyles>แหล่งข้อมูล</TitleCeneterStyles>
        </Col>
        <Col lg={12}>
        <p>ลดเวลาเรียนรู้การทำบัญชีด้วยตนเอง เพราะระบบจะนำทุกเอกสารที่เปิดมาบันทึกบัญชีแยกประเภทให้โดยอัตโนมัติ ทำให้ได้ผลประกอบการที่สามารถดูได้ทุกวัน และรายงานต่างๆ ให้นักบัญชีเตรียมทำเอกสารสำหรับยื่นภาษีต่อไป</p>
        </Col>
        
        <Col lg={2}>
            <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-screen-desktop m-auto text-primary"></i>
              </div>
              <h4>เอกสารเชิงเทคนิค</h4>
            </div>
          </Col>
          <Col lg={2}>
            <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-layers m-auto text-primary"></i>
              </div>
              <h4>เทรนนิ่ง</h4>
            </div>
          </Col>
          <Col lg={2}>
            <div className="features-icons-item mx-auto mb-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-check m-auto text-primary"></i>
              </div>
              <h4>โปรโมชั่น</h4>
            </div>
          </Col>
          <Col lg={2}>
            <div className="features-icons-item mx-auto mb-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-check m-auto text-primary"></i>
              </div>
              <h4>การติดตั้ง</h4>
            </div>
          </Col>
          <Col lg={2}>
            <div className="features-icons-item mx-auto mb-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-check m-auto text-primary"></i>
              </div>
              <h4>ติดต่อเรา</h4>
            </div>
          </Col>
          <Col lg={2}>
            <div className="features-icons-item mx-auto mb-0 mb-lg-3">
              <div className="features-icons-icon d-flex">
                <i className="icon-check m-auto text-primary"></i>
              </div>
              <h4>คุณภาพสูง</h4>
            </div>
          </Col>
      </Row>
    </Container>
  </div>
  
)

export default IndexPage
