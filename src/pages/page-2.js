import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { Container, Row } from 'react-bootstrap';

const SecondPage = () => (
  <Layout>
    <SEO title="Page two" />
    <Container>
      <Row>
        <h1>สายเคเบิล</h1>
      </Row>
      <Row>
      <h2>สายเคเบิล</h2>
      </Row>
      <Row>
      <p>เกี่ยวกับเรา</p>
      </Row>
      <Row>
        sds
      </Row>
      <Row>
        <Link to="/page-2/">ไปที่หน้า 2</Link>
      </Row>
    </Container>
  </Layout>
)

export default SecondPage
