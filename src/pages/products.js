import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.css';

const RowFirstComponent = styled(Row)`
  margin-top: 1.45rem;
`;

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    
    <Container>
      <RowFirstComponent>
        <Col md={8}>
          <div className="card mb-4">
            <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
            <div className="card-body">
              <h2 className="card-title">ภาพรวมบริษัท</h2>
              <p className="card-text">เช็กยอดขาย และรายจ่ายในแต่ละวันแบบเข้าใจง่ายผ่านกราฟ เพียงสร้างเอกสารซื้อ-ขายในระบบอย่างสม่ำเสมอ ก็จะทำให้รู้สุขภาพการเงินแล้ววางแผนบริหารธุรกิจต่อได้ทันที</p>
              <a href="#" className="btn btn-primary">อ่านต่อ &rarr;</a>
            </div>
            <div className="card-footer text-muted">
              ลงวันที่ 20 ส.ค. 2562
            </div>
          </div>
          <div className="card mb-4">
            <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
            <div className="card-body">
              <h2 className="card-title">ภาพรวมบริษัท</h2>
              <p className="card-text">เช็กยอดขาย และรายจ่ายในแต่ละวันแบบเข้าใจง่ายผ่านกราฟ เพียงสร้างเอกสารซื้อ-ขายในระบบอย่างสม่ำเสมอ ก็จะทำให้รู้สุขภาพการเงินแล้ววางแผนบริหารธุรกิจต่อได้ทันที</p>
              <a href="#" className="btn btn-primary">อ่านต่อ &rarr;</a>
            </div>
            <div className="card-footer text-muted">
              ลงวันที่ 20 ส.ค. 2562
            </div>
          </div>
          <div className="card mb-4">
            <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
            <div className="card-body">
              <h2 className="card-title">ภาพรวมบริษัท</h2>
              <p className="card-text">เช็กยอดขาย และรายจ่ายในแต่ละวันแบบเข้าใจง่ายผ่านกราฟ เพียงสร้างเอกสารซื้อ-ขายในระบบอย่างสม่ำเสมอ ก็จะทำให้รู้สุขภาพการเงินแล้ววางแผนบริหารธุรกิจต่อได้ทันที</p>
              <a href="#" className="btn btn-primary">อ่านต่อ &rarr;</a>
            </div>
            <div className="card-footer text-muted">
              ลงวันที่ 20 ส.ค. 2562
            </div>
          </div>
          <div className="card mb-4">
            <img className="card-img-top" src="http://placehold.it/750x300" alt="Card image cap" />
            <div className="card-body">
              <h2 className="card-title">ภาพรวมบริษัท</h2>
              <p className="card-text">เช็กยอดขาย และรายจ่ายในแต่ละวันแบบเข้าใจง่ายผ่านกราฟ เพียงสร้างเอกสารซื้อ-ขายในระบบอย่างสม่ำเสมอ ก็จะทำให้รู้สุขภาพการเงินแล้ววางแผนบริหารธุรกิจต่อได้ทันที</p>
              <a href="#" className="btn btn-primary">อ่านต่อ &rarr;</a>
            </div>
            <div className="card-footer text-muted">
              ลงวันที่ 20 ส.ค. 2562
            </div>
          </div>
        </Col>
        <Col md={4}>
          <div className="card mb-4">
            <h5 className="card-header">ภาพรวมสินค้า</h5>
            <div className="card-body">
              <div className="input-group">
                <input type="text" className="form-control" placeholder="" />
                <span className="input-group-btn">
                  <button className="btn btn-secondary" type="button">ค้นหา</button>
                </span>
              </div>
            </div>
          </div>
          <Link to="/page-2/">ไปที่หน้า 2</Link>
        </Col>
      </RowFirstComponent>
    </Container>

    
  </Layout>
)

export default IndexPage
